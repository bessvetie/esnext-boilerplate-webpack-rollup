const uaParser = require('ua-parser-js');
const path = require('path');
const fs = require('fs-extra');

module.exports = ({ app, nunjucks, logger }) => {
  app.get('/*', (req, res) => {
    const ua = uaParser(req.headers['user-agent']);

    let manifest = fs.readdirSync(app.get('publicPath')).find((file) => (
      file.includes('manifest')
    ));

    logger.log(manifest);

    manifest = fs.readJsonSync(
      path.join(app.get('publicPath'), `/${manifest}`)
    );

    const templateData = {
      manifest,
      browserSupportsModulePreload: ua.engine.name === 'Blink',
      ENV: process.env.NODE_ENV || 'development'
    };

    res.send(nunjucks.render('views/index.html', templateData));
  });

  return app;
};
