const express = require('express');
const nunjucks = require('nunjucks');
const path = require('path');
const fs = require('fs');
const cookieParser = require('cookie-parser');

const pkg = require('../package.json');

module.exports = (logger, autoLogger) => {
  const publicPath = path.join(__dirname, `../${pkg.config.publicDir}`);

  nunjucks.configure({
    noCache: process.env.NODE_ENV !== 'production'
  });

  const app = express();
  app.set('publicPath', publicPath);

  app.use(autoLogger);
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(express.static(publicPath));

  // dynamic loading of routes
  function recursiveRoutes(folderName) {
    const folder = path.join(__dirname, folderName);
    fs.readdirSync(folder)
      .forEach((item) => {
        const fullName = path.join(folderName, item);
        const stat = fs.lstatSync(path.join(folder, item));

        if (stat.isDirectory()) {
          recursiveRoutes(fullName);
        }
        else if (path.extname(fullName) === '.js') {
          const file = `.${path.sep}${fullName}`;
          logger.log(fullName);

          require(file)({
            app,
            nunjucks,
            logger
          });
        }
      });
  }
  recursiveRoutes('routes');

  return app;
};
