import React from 'react';
import { Container } from 'reactstrap';

import logo from '../../assets/images/logo.jpeg';

export default function DashboardContent() {
  return (
    <Container className="dashboard-content">
      <div className="d-flex align-content-center h-100">
        <img className="w-100" style={{ objectFit: 'contain' }} src={logo} alt="" />
      </div>
    </Container>
  );
}
