import React from 'react';

import Layout1 from '../layouts/Layout1';
import DashboardContent from '../components/Dashboard/DashboardContent';

function Index() {
  return (
    <DashboardContent />
  );
}

export default Layout1(Index);
