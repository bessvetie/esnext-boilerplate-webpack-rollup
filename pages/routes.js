import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import index from './index';

export default function routes() {
  return (
    <Switch>
      <Route exact path="/dashboard" component={index} />

      <Redirect exact from="/" to="/dashboard" />
      <Redirect from="*" to="/" />
    </Switch>
  );
}
