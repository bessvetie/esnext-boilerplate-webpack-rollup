import React from 'react';
import ReactDOM from 'react-dom';
import Root from './Root';
import routes from './routes';

import '../assets/scss/index.scss';

export default function renderApp() {
  ReactDOM.render(
    <Root routes={routes} />,
    document.getElementById('app')
  );
}

renderApp();
