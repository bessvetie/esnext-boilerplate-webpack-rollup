import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

Root.propTypes = {
  routes: PropTypes.func.isRequired
};

export default function Root({ routes }) {
  return (
    <BrowserRouter>
      {routes()}
    </BrowserRouter>
  );
}
