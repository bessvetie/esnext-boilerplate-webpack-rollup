import React from 'react';
import { Container } from 'reactstrap';

import Header from './Header';
import Footer from './Footer';

export default function Layout1(WrappedComponent) {
  return function Component(props) {
    return (
      <Container fluid className="p-0">
        <Header />

        <Container fluid className="page">
          <WrappedComponent {...props} />
        </Container>

        <Footer />
      </Container>
    );
  };
}
