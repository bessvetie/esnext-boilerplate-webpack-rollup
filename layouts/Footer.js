import React from 'react';
import { Container } from 'reactstrap';

export default function Footer() {
  return (
    <footer className="bg-dark py-3">
      <Container fluid className="d-flex justify-content-end">
        <a href="https://threejs.org" className="text-light font-weight-light">
          threejs.org
        </a>
      </Container>
    </footer>
  );
}
