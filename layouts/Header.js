import React from 'react';
import {
  Navbar, Nav, NavItem, NavLink, NavbarBrand
} from 'reactstrap';

export default function Header() {
  return (
    <Navbar className="navbar-main bg-light border-bottom py-2 px-0" light expand="sm">
      <NavbarBrand className="text-uppercase font-weight-bold px-3" href="/">
        React
      </NavbarBrand>

      <Nav className="position-absolute w-100 justify-content-center text-uppercase" navbar>
        <NavItem>
          <NavLink href="/">Dashboard</NavLink>
        </NavItem>
      </Nav>
    </Navbar>
  );
}
